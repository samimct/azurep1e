#!/bin/python

from flask import Flask, jsonify
import socket

app = Flask(__name__)
h = socket.gethostname()
u = '<h1>Backend: {0}</h1>'.format(h)
t = [
    {
        'id': 1,
        'vm': u,
        'description': u'This is From Backend Layer',
    }
]
print(t)
@app.route('/cvm/api/v1.0/t', methods=['GET'])
def get_tasks():
    return jsonify({'t': t})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')


